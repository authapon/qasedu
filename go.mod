module gitlab.com/authapon/qasedu

go 1.16

require (
	github.com/mattn/go-sqlite3 v1.14.6 // indirect
	gitlab.com/authapon/corpus v1.0.0
	gitlab.com/authapon/moosqlite v1.0.0 // indirect
	gitlab.com/authapon/qascrflib v1.0.0
)
