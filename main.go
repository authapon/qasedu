package qasedu

import (
	"fmt"
	"strings"

	"gitlab.com/authapon/corpus"
	crflib "gitlab.com/authapon/qascrflib"
)

var (
	EDUpattern1 = []string{
		"VRBpat:PRPpat:NP::VRBpat",
		"VRBpat:NP::VRIpat",
		"VRBpat:NP:ADVpat:PRPpat:NP::VRIpat",
		"VRIpat::PRPpat:VRBpat",
		"VRIpat:PRPpat:NP::PRL:VRBpat",
		"VRBpat:NP:ADVpat::PRPpat:VRBpat",
		"VRBpat:NP::PRL:VRBpat",
		"VRBpat:PRPpat:NP:ADVpat::VRBpat",
		"VRBpat:PRPpat:NP:CON:NP::NP:VRBpat",
		"VRBpat:NP:PRL:ADJpat::VRIpat",
		"VRIpat:PRPpat:TIMEpat::VRBpat",
		"VRBpat:NP:PRL:ADJpat:PRPpat:NP::VRBpat",
		"VRBpat:NP::DRF",
		"VRBpat:PRPpat:NP:CON:NP:ADVpat::PRL:VRBpat",
		"VRBpat:AMTpat::VRBpat",
		"VRBpat:NP::CON:VRIpat",
		"VRBpat:NP:CON:AMTpat::VRBpat",
		"VRBpat::PRPpat:VRIpat",
		"VRBpat:PRPpat:TIMEpat:PRPpat:NP::VRBpat",
		"VRBpat::CON:VRBpat",
		"VRBpat:NP:CON:NP::VRBpat",
		"VRBpat:NP:CON:ADJpat::VRBpat",
		"VRBpat:ADVpat:comma::ADJpat:AMTpat:VRBpat",
		"VRBpat:NP::CON:VRBpat",
		"VRBpat:NP:PRL:ADJpat:PRPpat:NP::PRL:VRBpat",
		"VRBpat:NP:ADVpat:PRPpat::VRBpat",
		"NP2::NP:VRBpat",
		"VRBpat:PRPpat:NP::CON:VRIpat",
		"VRBpat:PRPpat:NP::PRPpat:NP:VRBpat",
		"VRBpat:NP:ADVpat::NP:VRBpat",
		"VRBpat:NP:PRL:ADJpat:CON:NP::PRL:VRBpat",
		"VRBpat:NP:PRL:ADJpat:ADVpat::VRBpat",
		"VRBpat:NP:PRL:ADJpat:PRPpat:AMTpat::CON:VRBpat",
		"VRIpat:PRPpat:NP::PRL:VRIpat",
		"VRBpat:NP:CON:NP:CON:NP::VRBpat",
		"VRBpat:PRPpat:AMTpat::PRL:VRBpat",
		"VRBpat:NP:ADVpat:PRPpat:CLSpat:AMTpat::CON:NP:VRBpat",
		"VRBpat:NP:ADVpat::NP:PRL:VRBpat",
		"VRBpat:NP:ADVpat::VRBpat",
		"VRBpat:PRPpat:NP:NP::VRBpat",
		"VRBpat:NP:CON:NP::PRPpat:VRBpat",
		"VRBpat:PRPpat:NP::CON:VRBpat",
		"VRBpat:NP::NP:VRBpat",
		"VRBpat:ADVpat:PRPpat:NP::PRL:VRBpat",
		"VRBpat:NP::PRPpat:VRBpat:NP",
		"VRBpat:NP:ADVpat::CON:VRBpat",
		"VRBpat:NP::VRBpat",
		"VRBpat:NP::NP:CON:NP:ADVpat:VRBpat",
		"VRBpat:ADVpat:PRPpat:NP::PRL:NP:VRBpat",
		"VRBpat:NP::PRL:VRIpat",
		"VRBpat:PRPpat:NP:PRL:ADJpat:CON:NP::PRL:VRBpat",
		"VRBpat:NP:TIMEpat::VRBpat",
		"VRBpat:NP:PRL:ADJpat:PRPpat:NP::PRL:NP:VRBpat",
		"VRBpat:TIMEpat::PRL:NP:VRBpat",
		"VRBpat:PRPpat:NP:CON:NP:CON:PRPpat:NP::VRBpat",
		"VRIpat::PRL:VRBpat",
		"VRBpat:ADVpat::VRIpat",
		"VRBpat:AMTpat:PRPpat:NP::PRL:VRBpat",
		"VRBpat:PRPpat:NP:CON:NP::PRL:VRBpat",
		"VRBpat:NP:ADVpat:NP::VRBpat",
		"VRBpat:NP::PRPpat:VRIpat",
		"VRBpat:NP:PRL:ADJpat::VRBpat",
		"VRBpat:NP::VRBpat:NP:VRBpat",
		"VRBpat:PRPpat:NP::PRL:VRBpat",
		"VRBpat:NP::CON:PRPpat:VRBpat",
		"VRBpat::VRIpat",
		"VRBpat:ADVpat::PRPpat:VRBpat",
		"VRBpat:PRPpat:TIMEpat::PRL:VRBpat",
		"UNK:quotation::NP:VRBpat",
		"NP2::CON:VRBpat",
		"VRBpat:PRPpat:TIMEpat:PRPpat:NP::PRL:VRBpat",
		"VRBpat:PRPpat:NP::PRL:NP:VRIpat",
		"VRBpat::PRPpat:VRBpat",
		"VRBpat:NP::PRPpat:VRBpat",
		"VRBpat::CON:VRIpat",
		"VRBpat:NP::PRL:NP:VRBpat",
		"VRBpat:NP:PRPpat:PRPpat:NP::PRL:VRIpat",
		"VRBpat:PRPpat:NP:PRL:ADJpat::NP:VRBpat",
		"VRBpat:NP::NP2",
		"VRBpat:PRPpat:TIMEpat::VRBpat",
		"VRIpat::CON:VRBpat",
		"VRBpat:NP:ADVpat::NP",
		"VRBpat:AMTpat::CON:VRBpat",
		"VRIpat::VRBpat",
		"VRBpat:PRPpat:NP:ADVpat::PRL:VRBpat",
		"VRBpat:NP:CON:NP::PRL:VRBpat",
		"VRBpat:PRPpat:NP::PRL:VRIpat",
		"VRBpat:ADVpat::CON:VRBpat",
		"VRBpat:NP:ADJpat:PRPpat::PRL:NP:VRBpat",
		"VRBpat:NP:CON:NP:ADVpat::PRPpat:VRBpat",
		"VRBpat:PRPpat:NP:CON:NP::VRBpat",
		"VRBpat:ADVpat::VRBpat",
		"VRBpat:NP::PRPpat:quotation:UNK",
		"VRIpat:ADVpat::PRPpat:VRBpat",
		"VRBpat:NP:ADVpat::PRL:VRIpat",
		"VRBpat:NP:ADVpat:PRPpat:NP::VRBpat",
		"VRBpat:ADVpat::comma:NP:VRBpat",
		"VRBpat:ADVpat:PRPpat:NP::NP:VRBpat",
		"VRBpat:NP:ADVpat:PRPpat:NP::PRL:VRIpat",
		"VRBpat:PRPpat:NP::PRPpat:VRBpat",
		"VRBpat:ADVpat::PRPpat:VRIpat",
		"VRBpat:TIMEpat::VRBpat",
		"VRBpat:NP:PRL:ADJpat:PRPpat:NP::PRPpat:VRBpat",
		"VRBpat:NP::CON:NP:VRBpat",
		"VRBpat:ADVpat::NP:VRBpat",
		"VRBpat:NP:ADVpat::NP:VRIpat",
		"VRIpat::NP:VRBpat",
		"VRBpat:PRPpat:NP::NP:VRBpat",
		"VRBpat::VRBpat",
		"VRBpat:NP:CON:NP::VRIpat",
		"VRBpat:NP::NP:PRL:VRBpat",
		"VRBpat:ADVpat:AMTpat::VRBpat",
		"VRBpat:AMTpat:PRPpat:AMTpat:NP2::CON:VRBpat",
		"VRBpat:NP:CON:ADJpat::PRL:VRBpat",
	}
	EDUpattern2 = []string{
		"::DRF",
		"DRF::",
		"::DRE",
		"DRE::",
		"::SUB",
		"SUB::",
	}
)

func EDUConTest(label []string, index int, con string) bool {
	conX := strings.Split(con, ":")
	if len(label) < index+len(conX) {
		return false
	}
	match := true
	for i := range conX {
		if conX[i] != label[index+i] {
			match = false
			break
		}
	}
	return match
}

func EDUConTest2(label []string, index int, t1 []string, t2 []string) bool {
	if len(t1)+len(t2) > len(label)-index {
		return false
	}

	ix := index
	for i := range t1 {
		if t1[i] != label[ix] {
			return false
		}
		ix++
	}
	for i := range t2 {
		if t2[i] != label[ix] {
			return false
		}
		ix++
	}

	return true
}

func EDUSegByTag(data string, tag string, cut bool) string {
	datax := crflib.GetWordTag(data)
	if len(datax.Sentence) == 0 {
		return ""
	}

	sizeTag := len(strings.Split(tag, ":"))
	dataOut := ""
	startIndex := 0
	index := 0
	for {
		if EDUConTest(datax.Label, index, tag) {
			if tag == "space" && index > 0 && index+1 < len(datax.Label) && datax.Label[index-1] == "UNK" && datax.Label[index+1] == "UNK" {
				fmt.Printf("Exception\n")
				index++
			} else {
				if index > 0 && index+1 < len(datax.Label) {
					fmt.Printf("EDU Work %s %s %s\n", datax.Label[index-1], tag, datax.Label[index+1])
				} else {
					fmt.Printf("EDU Work %s\n", tag)
				}
				xgen := crflib.CrfTrainSentence{}
				if cut {
					xgen.Sentence = datax.Sentence[startIndex:index]
					xgen.Label = datax.Label[startIndex:index]
					dataOut += crflib.WordTagString(xgen) + "\n"
					index += sizeTag
					startIndex = index
				} else {
					if index == 0 {
						index += sizeTag
					} else {
						xgen.Sentence = datax.Sentence[startIndex:index]
						xgen.Label = datax.Label[startIndex:index]
						dataOut += crflib.WordTagString(xgen) + "\n"
						startIndex = index
						index += sizeTag
					}
				}
			}
		} else {
			index++
		}

		if index >= len(datax.Label) {
			xgen := crflib.CrfTrainSentence{}
			xgen.Sentence = datax.Sentence[startIndex:]
			xgen.Label = datax.Label[startIndex:]
			dataOut += crflib.WordTagString(xgen) + "\n"
			break
		}
	}
	return dataOut
}

func EDUSegByTag2(data string, tag string) string {
	fmt.Printf("Start work for %s\n", tag)
	dd := EncapNP(data)
	fmt.Printf("Working on : %s\n", dd)
	datax := crflib.GetWordTag(dd)
	if len(datax.Sentence) == 0 {
		return ""
	}

	tagX := strings.Split(tag, "::")
	if len(tagX) != 2 {
		fmt.Printf("No pattern\n")
		return ""
	}
	tagX0 := strings.Split(tagX[0], ":")
	tagX1 := strings.Split(tagX[1], ":")

	if tagX0[0] == "" {
		tagX0 = make([]string, 0)
	}
	if tagX1[0] == "" {
		tagX1 = make([]string, 0)
	}

	dataOut := ""
	startIndex := 0
	index := 0
	for {
		if EDUConTest2(datax.Label, index, tagX0, tagX1) {
			fmt.Printf("EDU Segment for %s\n", tag)
			xgen := crflib.CrfTrainSentence{}
			xgen.Sentence = datax.Sentence[startIndex : index+len(tagX0)]
			xgen.Label = datax.Label[startIndex : index+len(tagX0)]
			dataOut += crflib.WordTagString(xgen) + "\n"
			if len(tagX0) == 0 {
				startIndex = index
				index++
			} else {
				index = index + len(tagX0)
				startIndex = index
			}

		} else {
			index++
		}

		if index >= len(datax.Label) {
			xgen := crflib.CrfTrainSentence{}
			xgen.Sentence = datax.Sentence[startIndex:]
			xgen.Label = datax.Label[startIndex:]
			dataOut += crflib.WordTagString(xgen) + "\n"
			break
		}
	}
	dataOut2 := ""
	datax2 := strings.Split(dataOut, "\n")
	for i := range datax2 {
		dataOut2 += DecapNP(datax2[i]) + "\n"
	}

	return dataOut2
}

func DecapNP(data string) string {
	dataout := crflib.CrfTrainSentence{}
	dataout.Sentence = []string{}
	dataout.Label = []string{}

	datax := crflib.GetWordTag(data)

	for i := range datax.Label {
		switch datax.Label[i] {
		case "NP", "NP2":
			dataxx := crflib.GetWordTag(datax.Sentence[i])
			for ii := range dataxx.Label {
				dataout.Sentence = append(dataout.Sentence, dataxx.Sentence[ii])
				dataout.Label = append(dataout.Label, dataxx.Label[ii])
			}
		default:
			dataout.Sentence = append(dataout.Sentence, datax.Sentence[i])
			dataout.Label = append(dataout.Label, datax.Label[i])
		}
	}
	return crflib.WordTagString(dataout)
}

func EncapNP(data string) string {
	datax := EncapNP2(crflib.GetWordTag(data))
	datax = EncapNP1(datax)

	return crflib.WordTagString(datax)
}

func wordTagPartial(data crflib.CrfTrainSentence, start int, last int) string {
	dx := crflib.CrfTrainSentence{}
	dx.Label = data.Label[start:last]
	dx.Sentence = data.Sentence[start:last]
	return crflib.WordTagString(dx)

}

func EncapNP1(data crflib.CrfTrainSentence) crflib.CrfTrainSentence {
	dataout := crflib.CrfTrainSentence{}
	dataout.Sentence = []string{}
	dataout.Label = []string{}
	state := "normal"
	start := 0
	index := 0
	for {
		if index >= len(data.Label) {
			if state == "NP" {
				dataout.Sentence = append(dataout.Sentence, wordTagPartial(data, start, index))
				dataout.Label = append(dataout.Label, "NP")
			}
			return dataout
		}
		switch state {
		case "normal":
			switch data.Label[index] {
			case "HNpat", "DETpat", "VNNpat":
				state = "NP"
				start = index
			default:
				dataout.Sentence = append(dataout.Sentence, data.Sentence[index])
				dataout.Label = append(dataout.Label, data.Label[index])
			}
		case "NP":
			switch data.Label[index-1] {
			case "PRPpat":
				switch data.Label[index] {
				case "HNpat", "DETpat", "TIMEpat", "AMTpat", "VNNpat":
				default:
					dataout.Sentence = append(dataout.Sentence, wordTagPartial(data, start, index-1))
					dataout.Label = append(dataout.Label, "NP")
					dataout.Sentence = append(dataout.Sentence, data.Sentence[index-1])
					dataout.Label = append(dataout.Label, "PRPpat")
					state = "normal"
					index -= 1
				}
			case "VNNpat":
				switch data.Label[index] {
				case "HNpat", "DETpat", "TIMEpat", "VNNpat", "PRPpat":
				default:
					dataout.Sentence = append(dataout.Sentence, wordTagPartial(data, start, index))
					dataout.Label = append(dataout.Label, "NP")
					state = "normal"
					index -= 1
				}
			case "TIMEpat":
				switch data.Label[index] {
				case "AMTpat", "TIMEpat", "PRPpat":
				default:
					dataout.Sentence = append(dataout.Sentence, wordTagPartial(data, start, index))
					dataout.Label = append(dataout.Label, "NP")
					state = "normal"
					index -= 1
				}
			case "HNpat":
				switch data.Label[index] {
				case "ADJpat", "DETpat", "VNNpat", "AMTpat", "PRPpat":
				default:
					dataout.Sentence = append(dataout.Sentence, wordTagPartial(data, start, index))
					dataout.Label = append(dataout.Label, "NP")
					state = "normal"
					index -= 1
				}
			case "AMTpat":
				switch data.Label[index] {
				case "TIMEpat", "PRPpat":
				default:
					dataout.Sentence = append(dataout.Sentence, wordTagPartial(data, start, index))
					dataout.Label = append(dataout.Label, "NP")
					state = "normal"
					index -= 1
				}
			case "ADJpat":
				switch data.Label[index] {
				case "DETpat", "AMTpat", "PRPpat":
				default:
					dataout.Sentence = append(dataout.Sentence, wordTagPartial(data, start, index))
					dataout.Label = append(dataout.Label, "NP")
					state = "normal"
					index -= 1
				}
			case "DETpat":
				switch data.Label[index] {
				case "AMTpat", "ADJpat", "PRPpat":
				default:
					dataout.Sentence = append(dataout.Sentence, wordTagPartial(data, start, index))
					dataout.Label = append(dataout.Label, "NP")
					state = "normal"
					index -= 1
				}
			}
		}
		index += 1
	}
}

func EncapNP2(data crflib.CrfTrainSentence) crflib.CrfTrainSentence {
	dataout := crflib.CrfTrainSentence{}
	dataout.Label = []string{}
	dataout.Sentence = []string{}
	state := "normal"
	start := 0
	index := 0
	for {
		if index >= len(data.Label) {
			if state == "NP2" {
				switch data.Label[index-1] {
				case "ADJpat", "ADVpat":
					dataout.Sentence = append(dataout.Sentence, wordTagPartial(data, start, index))
					dataout.Label = append(dataout.Label, "NP2")
				default:
					dataout.Sentence = append(dataout.Sentence, data.Sentence[start:index]...)
					dataout.Label = append(dataout.Label, data.Label[start:index]...)
				}
			}
			return dataout
		}
		switch state {
		case "normal":
			switch data.Label[index] {
			case "HNpat":
				if index == 0 {
					state = "NP2"
					start = index
				} else {
					switch data.Label[index-1] {
					case "PRPpat", "VRBpat", "VNNpat", "PRL", "CON":
						dataout.Sentence = append(dataout.Sentence, data.Sentence[index])
						dataout.Label = append(dataout.Label, data.Label[index])
					default:
						state = "NP2"
						start = index
					}
				}
			default:
				dataout.Sentence = append(dataout.Sentence, data.Sentence[index])
				dataout.Label = append(dataout.Label, data.Label[index])
			}
		case "NP2":
			switch data.Label[index] {
			case "ADJpat":
				switch data.Label[index-1] {
				case "HNpat":
				default:
					dataout.Sentence = append(dataout.Sentence, data.Sentence[start:index+1]...)
					dataout.Label = append(dataout.Label, data.Label[start:index+1]...)
					state = "normal"
				}
			case "ADVpat":
				switch data.Label[index-1] {
				case "ADJpat":
				default:
					dataout.Sentence = append(dataout.Sentence, data.Sentence[start:index+1]...)
					dataout.Label = append(dataout.Label, data.Label[start:index+1]...)
					state = "normal"
				}
			default:
				switch data.Label[index-1] {
				case "ADJpat", "ADVpat":
					switch data.Label[index] {
					case "HNpat", "CON":
						dataout.Sentence = append(dataout.Sentence, wordTagPartial(data, start, index))
						dataout.Label = append(dataout.Label, "NP2")
						state = "normal"
						index -= 1
					default:
						dataout.Sentence = append(dataout.Sentence, data.Sentence[start:index]...)
						dataout.Label = append(dataout.Label, data.Label[start:index]...)
						state = "normal"
						index -= 1
					}
				default:
					dataout.Sentence = append(dataout.Sentence, data.Sentence[start:index]...)
					dataout.Label = append(dataout.Label, data.Label[start:index]...)
					state = "normal"
					index -= 1
				}
			}
		}
		index += 1
	}
}

func RemoveParenthesis(data string) string {
	datax := crflib.GetWordTag(data)
	if len(datax.Sentence) == 0 {
		return ""
	}

	dataOut := ""
	startIndex := 0
	leftParen := false
	leftIndex := 0
	index := 0
	for {
		if !leftParen {
			if datax.Label[index] == "left_parenthesis" {
				leftIndex = index
				leftParen = true
				index++
			} else {
				index++
			}
		} else {
			if datax.Label[index] == "right_parenthesis" {
				xgen := crflib.CrfTrainSentence{}
				xgen.Sentence = datax.Sentence[startIndex:leftIndex]
				xgen.Label = datax.Label[startIndex:leftIndex]
				dataOut += crflib.WordTagString(xgen)
				if leftIndex > 0 && index+1 < len(datax.Label) && (datax.Label[leftIndex-1] == "NCM" || datax.Label[leftIndex-1] == "NPN") && (datax.Label[index+1] == "NCM" || datax.Label[index+1] == "NPN") {
					dataOut += "<space>"
				}
				index++
				startIndex = index
				leftParen = false
			} else {
				index++
			}
		}

		if index >= len(datax.Label) {
			xgen := crflib.CrfTrainSentence{}
			xgen.Sentence = datax.Sentence[startIndex:]
			xgen.Label = datax.Label[startIndex:]
			dataOut += crflib.WordTagString(xgen) + "\n"
			break
		}
	}
	return dataOut
}

func EDUSegment(data string) string {
	datax := strings.Split(data, "\n")
	dataOut := ""
	for i := range datax {
		dataOut += RemoveParenthesis(datax[i])
	}

	datax = strings.Split(dataOut, "\n")
	dataOut = ""
	for i := range datax {
		dataOut += EDUSegByTag(crflib.StringWordTag(crflib.NormalizeSentence(crflib.GetWordTag(datax[i]))), "space", true)
	}

	datax = strings.Split(dataOut, "\n")
	dataOut = ""
	for i := range datax {
		dataOut += EDUSegByTag(crflib.StringWordTag(crflib.NormalizeSentence(crflib.GetWordTag(datax[i]))), "SUB", false)
	}

	datax = strings.Split(dataOut, "\n")
	dataOut = ""
	for i := range datax {
		dataOut += EDUSegByTag(crflib.StringWordTag(crflib.NormalizeSentence(crflib.GetWordTag(datax[i]))), "end", false)
	}

	return corpus.CleanSentence(dataOut)
}

func EDUSegment2(data string, pattern []string) string {
	datax := strings.Split(data, "\n")
	dataOut := ""
	for i := range datax {
		dataOut += RemoveParenthesis(datax[i])
	}

	for ix := range pattern {

		datax = strings.Split(dataOut, "\n")
		dataOut = ""
		for i := range datax {
			dataOut += EDUSegByTag2(crflib.StringWordTag(crflib.NormalizeSentence(crflib.GetWordTag(datax[i]))), pattern[ix])
		}
	}

	return corpus.CleanSentence(dataOut)
}

func EDUcombine(data string, cond1 func(string) bool, bodyCond func(string) bool, cond2 func(string) (bool, bool)) string {
	lines := strings.Split(data, "\n")
	lineOut := make([]string, 0)
	buffers := make([]string, 0)
	combineState := false
	i := 0
	start := 0
	for {
		fmt.Printf("Line %d - %s\n", i, lines[i])
		if !combineState {
			if cond1(lines[i]) {
				fmt.Printf("Start ...\n%s\n", lines[i])
				combineState = true
				buffers = make([]string, 0)
				buffers = append(buffers, lines[i])
				start = i
			} else {
				lineOut = append(lineOut, lines[i])
			}
		} else {
			if bodyCond(lines[i]) {
				fmt.Printf("Body ...\n%s\n", lines[i])
				buffers = append(buffers, lines[i])
			} else {
				con, bound := cond2(lines[i])
				combineState = false
				if !con {
					fmt.Printf("Cancel ...\n%s\n", lines[i])
					i = start
					lineOut = append(lineOut, lines[i])
				} else {
					fmt.Printf("Stop ...\n%s\n", lines[i])
					lineBuffer := ""
					for _, buffer := range buffers {
						lineBuffer += buffer
					}
					if bound {
						lineBuffer += lines[i]
						lineOut = append(lineOut, lineBuffer)
					} else {
						lineOut = append(lineOut, lineBuffer)
						i -= 1
					}
				}
			}
		}
		i += 1
		if i >= len(lines) {
			if combineState == true {
				con, _ := cond2("")
				combineState = false
				if !con {
					fmt.Printf("Cancel ...\n%s\n", lines[start])
					i = start
					lineOut = append(lineOut, lines[i])
					i += 1
					if i >= len(lines) {
						break
					}
				} else {
					fmt.Printf("Stop Lastline...\n")
					lineBuffer := ""
					for _, buffer := range buffers {
						lineBuffer += buffer
					}
					lineOut = append(lineOut, lineBuffer)
					break
				}
			} else {
				break
			}
		}
	}
	outputLine := ""
	for i, v := range lineOut {
		switch i {
		case 0:
			outputLine = v
		default:
			outputLine += "\n" + v
		}
	}
	return outputLine
}

func IsNP(data string) bool {
	wdtag := crflib.GetWordTag(EncapNP(data))
	if len(wdtag.Label) == 0 {
		return false
	}
	state := 0
	switch wdtag.Label[0] {
	case "CON", "comma":
		state = 1
	case "NP":
		state = 2
		if len(wdtag.Label) == 1 {
			return true
		}
	}

	if state == 0 {
		return false
	}

	for i := range wdtag.Label {
		if i == 0 {
			continue
		}

		switch state {
		case 1:
			switch wdtag.Label[i] {
			case "NP":
				state = 2
			default:
				return false
			}
		case 2:
			switch wdtag.Label[i] {
			case "CON", "comma":
				state = 1
			default:
				return false
			}

		}
	}

	return true
}

func NotNPnbound(data string) (bool, bool) {
	if ParaEND(data) {
		return false, false
	}
	return !IsNP(data), false
}

func LineWith(data, tag string) bool {
	wtag := crflib.GetWordTag(data)
	if len(wtag.Label) != 1 {
		return false
	}
	if wtag.Label[0] != tag {
		return false
	}
	return true
}

func LineWithDRF(data string) bool {
	return LineWith(data, "DRF")
}

func LineWithDREbound(data string) (bool, bool) {
	if LineWith(data, "DRE") {
		return true, true
	}
	return false, false
}
func LineWithSUB(data string) bool {
	return LineWith(data, "SUB")
}

func NLineWithSUBbound(data string) (bool, bool) {
	if ParaEND(data) {
		return false, false
	}
	return !LineWith(data, "SUB"), true
}

func LineEndWith(data, tag string) bool {
	wtag := crflib.GetWordTag(data)
	if len(wtag.Label) == 0 {
		return false
	}
	if wtag.Label[len(wtag.Label)-1] == tag {
		return true
	}
	return false
}

func LineEndWithPRPpat(data string) bool {
	return LineEndWith(data, "PRPpat")
}

func LineStartWith(data, tag string) bool {
	wtag := crflib.GetWordTag(data)
	if len(wtag.Label) == 0 {
		return false
	}
	if wtag.Label[0] == tag {
		return true
	}
	return false
}

func LineStartWithVRBbound(data string) (bool, bool) {
	return LineStartWith(data, "VRBpat"), true
}

func LineStartWithVRIbound(data string) (bool, bool) {
	return LineStartWith(data, "VRIpat"), true
}

func LineEndWithVRB(data string) bool {
	return LineEndWith(data, "VRBpat")
}

func LineEndWithNP(data string) bool {
	if IsNP(data) {
		return false
	}
	wtag := crflib.GetWordTag(EncapNP(data))
	if len(wtag.Label) == 0 {
		return false
	}
	switch wtag.Label[len(wtag.Label)-1] {
	case "NP":
		return true
	}
	return false
}

func NoBody(data string) bool {
	return false
}

func ParaEND(data string) bool {
	datax := crflib.GetWordTag(data)
	if datax.Label[0] == "end" {
		return true
	}
	return false
}

func ANY(data string) bool {
	if ParaEND(data) {
		return false
	}
	return true
}

func ANYbound(data string) (bool, bool) {
	if ANY(data) {
		return true, true
	}
	return false, false
}

func LineWithCON(data string) bool {
	return LineWith(data, "CON")
}

func LineStartWithSUBbound(data string) (bool, bool) {
	return LineStartWith(data, "SUB"), true
}

func EDUpatConvToNP(data string) string {
	datax := strings.Split(data, "::")
	if len(datax) != 2 {
		fmt.Printf("err -> %s\n", data)
		return ""
	}
	d0 := edupattoNP2(datax[0])
	d0 = edupattoNP(d0)
	d1 := edupattoNP2(datax[1])
	d1 = edupattoNP(d1)
	return d0 + "::" + d1
}

func edupattoNP(data string) string {
	datax := strings.Split(data, ":")
	dataout := make([]string, 0)
	state := "normal"
	index := 0
	for {
		if index >= len(datax) {
			dout := ""
			if state == "NP" {
				dataout = append(dataout, "NP")
			}
			for i := range dataout {
				switch i {
				case 0:
					dout += dataout[i]
				default:
					dout += ":" + dataout[i]
				}
			}
			return dout
		}
		switch state {
		case "normal":
			switch datax[index] {
			case "HNpat", "DETpat", "VNNpat":
				state = "NP"
			default:
				dataout = append(dataout, datax[index])
			}
		case "NP":
			switch datax[index-1] {
			case "PRPpat":
				switch datax[index] {
				case "HNpat", "DETpat", "TIMEpat", "AMTpat", "VNNpat":
				default:
					dataout = append(dataout, "NP")
					dataout = append(dataout, "PRPpat")
					state = "normal"
					index -= 1
				}
			case "VNNpat":
				switch datax[index] {
				case "HNpat", "DETpat", "TIMEpat", "VNNpat", "PRPpat":
				default:
					dataout = append(dataout, "NP")
					state = "normal"
					index -= 1
				}
			case "TIMEpat":
				switch datax[index] {
				case "AMTpat", "TIMEpat", "PRPpat":
				default:
					dataout = append(dataout, "NP")
					state = "normal"
					index -= 1
				}
			case "HNpat":
				switch datax[index] {
				case "ADJpat", "DETpat", "VNNpat", "AMTpat", "PRPpat":
				default:
					dataout = append(dataout, "NP")
					state = "normal"
					index -= 1
				}
			case "AMTpat":
				switch datax[index] {
				case "TIMEpat", "PRPpat":
				default:
					dataout = append(dataout, "NP")
					state = "normal"
					index -= 1
				}
			case "ADJpat":
				switch datax[index] {
				case "DETpat", "AMTpat", "PRPpat":
				default:
					dataout = append(dataout, "NP")
					state = "normal"
					index -= 1
				}
			case "DETpat":
				switch datax[index] {
				case "AMTpat", "ADJpat", "PRPpat":
				default:
					dataout = append(dataout, "NP")
					state = "normal"
					index -= 1
				}
			}
		}
		index += 1
	}
}

func edupattoNP2(data string) string {
	datax := strings.Split(data, ":")
	dataout := make([]string, 0)
	state := "normal"
	start := 0
	index := 0
	for {
		if index >= len(datax) {
			dout := ""
			if state == "NP2" {
				switch datax[index-1] {
				case "ADJpat", "ADVpat":
					dataout = append(dataout, "NP2")
				default:
					dataout = append(dataout, datax[start:index]...)
				}
			}
			for i := range dataout {
				switch i {
				case 0:
					dout += dataout[i]
				default:
					dout += ":" + dataout[i]
				}
			}
			return dout
		}
		switch state {
		case "normal":
			switch datax[index] {
			case "HNpat":
				if index == 0 {
					state = "NP2"
					start = index
				} else {
					switch datax[index-1] {
					case "PRPpat", "VRBpat", "VNNpat", "PRL", "CON":
						dataout = append(dataout, datax[index])
					default:
						state = "NP2"
						start = index
					}
				}
			default:
				dataout = append(dataout, datax[index])
			}
		case "NP2":
			switch datax[index] {
			case "ADJpat":
				switch datax[index-1] {
				case "HNpat":
				default:
					dataout = append(dataout, datax[start:index+1]...)
					state = "normal"
				}
			case "ADVpat":
				switch datax[index-1] {
				case "ADJpat":
				default:
					dataout = append(dataout, datax[start:index+1]...)
					state = "normal"
				}
			default:
				switch datax[index-1] {
				case "ADJpat", "ADVpat":
					switch datax[index] {
					case "HNpat", "CON":
						dataout = append(dataout, "NP2")
						state = "normal"
						index -= 1
					default:
						dataout = append(dataout, datax[start:index]...)
						state = "normal"
						index -= 1
					}
				default:
					dataout = append(dataout, datax[start:index]...)
					state = "normal"
					index -= 1
				}
			}
		}
		index += 1
	}
}
